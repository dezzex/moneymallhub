/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,BackHandler} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen'
import { Actions } from 'react-native-router-flux';

import Home from './src/component/Home';
import Main from './src/component/Main';
import CreditScore from './src/component/CreditScore';
import FinalCreditScore from './src/component/FinalCreditScore';
import ProductList from './src/component/ProductList';
import CardDetails from './src/component/CardDetails';
import BirthDetails from './src/component/BirthDetails';
import JoiningDetails from './src/component/JoiningDetails';
import AdditionalIncomeScreen from './src/component/AdditionalIncomeScreen';

import MartialDetails from './src/component/MartialDetails';
import LiabilitiesOption from './src/component/LiabilitiesOption';
import LoanDocuments from './src/component/LoanDocuments';

import EmployeeDetails from './src/component/EmployeeDetails';
import DigitalSignature from './src/component/SignatureScreen';
import UploadPicture from './src/component/UploadPicture';
import OrderConfirmation from './src/component/OrderConfirmation';
import TrackOrder from './src/component/TrackOrder';
import LoanList from './src/component/LoanList';
import LoanDetails from './src/component/LoanDetails';
import HomeLoanList from './src/component/HomeLoanList';
import HomeLoanDetails from './src/component/HomeLoanDetails';
import RegisterScreen from './src/component/RegisterScreen';
import VerificationScreen from './src/component/VerificationScreen';

// import {
//   createDrawerNavigator,
//   createStackNavigator,
//   createAppContainer,
// } from 'react-navigation';

// class NavigationDrawerStructure extends Component {
//   //Structure for the navigatin Drawer
//   toggleDrawer = () => {
//     //Props to open/close the drawer
//     this.props.navigationProps.toggleDrawer();
//   };
//   render() {
//     return (
//       <View style={{ flexDirection: 'row' }}>
//         <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
//           <Image
//             source={require('./src/image/drawer.png')}
//             style={{ width: 25, height: 25, marginLeft: 5 }}
//           />
//         </TouchableOpacity>
//       </View>
//     );
//   }
// }

// const FirstActivity_StackNavigator = createStackNavigator({
//   //All the screen from the Screen1 will be indexed here
//   First: {
//     screen: Main,
//     navigationOptions: ({ navigation }) => ({
//       title: 'MoneyMall',
//       headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
//       headerStyle: {
//         backgroundColor: '#FF9800',
//       },
//       headerTintColor: '#fff',
//     }),
//   },
// });

// const Screen2_StackNavigator = createStackNavigator({
//   //All the screen from the Screen2 will be indexed here
//   Second: {
//     screen: Home,
//     navigationOptions: ({ navigation }) => ({
//       title: 'MoneyMall',
//       headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
//       headerStyle: {
//         backgroundColor: '#FF9800',
//       },
//       headerTintColor: '#fff',
//     }),
//   },
// });

// const DrawerNavigatorExample = createDrawerNavigator({
//   //Drawer Optons and indexing
//   Screen1: {
//     //Title
//     screen: FirstActivity_StackNavigator,
//     navigationOptions: {
//       drawerLabel: 'Home',
//     },
//   },
//   Screen2: {
//     //Title
//     screen: Screen2_StackNavigator,
//     navigationOptions: {
//       drawerLabel: 'Main',
//     },
//   }
// });

// const RootStack = createStackNavigator({
//   Home: {
//     screen: Home
//   },
//   Main: {
//     screen: Main
//   }
// });

export default class App extends Component{
  
  componentDidMount(){
    SplashScreen.hide();
    BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPress.bind(this));
  }
  hardwareBackPress(){
    console.log('====================================');
    console.log("Hiii");
    console.log('====================================');
    Actions.pop();
     return true;
  }
  render() {
    return (
         
      <Router>
      <Scene key="root">
        <Scene key="RegisterScreen"  component={RegisterScreen} animation='fade' hideNavBar={true}   />
        <Scene key="VerificationScreen"  component={VerificationScreen} animation='fade' hideNavBar={true}   />
        <Scene key="Home"  component={Home} animation='fade' hideNavBar={true}  />
        <Scene key="Main"  component={Main} animation='fade' hideNavBar={true}   />
        <Scene key="CreditScore"  component={CreditScore} animation='fade' hideNavBar={true}  initial />
        <Scene key="FinalCreditScore"  component={FinalCreditScore} animation='fade' hideNavBar={true}   />
        <Scene key="ProductList"  component={ProductList} animation='fade' hideNavBar={true}   />
        <Scene key="CardDetails"  component={CardDetails} animation='fade' hideNavBar={true}   />
        <Scene key="EmployeeDetails"  component={EmployeeDetails} animation='fade' hideNavBar={true}   />
        <Scene key="BirthDetails"  component={BirthDetails} animation='fade' hideNavBar={true}   />
        <Scene key="JoiningDetails"  component={JoiningDetails} animation='fade' hideNavBar={true}   />
        <Scene key="AdditionalIncome"  component={AdditionalIncomeScreen} animation='fade' hideNavBar={true}  />
        
        <Scene key="MartialDetails"  component={MartialDetails} animation='fade' hideNavBar={true} />
        
        <Scene key="LiabilitiesOption"  component={LiabilitiesOption} animation='fade' hideNavBar={true} />
        <Scene key="LoanDocuments"  component={LoanDocuments} animation='fade' hideNavBar={true} />
        <Scene key="DigitalSignature"  component={DigitalSignature} animation='fade' hideNavBar={true}   />
        <Scene key="UploadPicture"  component={UploadPicture} animation='fade' hideNavBar={true} />
        <Scene key="OrderConfirmation"  component={OrderConfirmation} animation='fade' hideNavBar={true}  />
        <Scene key="TrackOrder"  component={TrackOrder} animation='fade' hideNavBar={true}    />
        <Scene key="LoanList"  component={LoanList} animation='fade' hideNavBar={true}    />
        <Scene key="LoanDetails"  component={LoanDetails} animation='fade' hideNavBar={true}    />
        <Scene key="HomeLoanList"  component={HomeLoanList} animation='fade' hideNavBar={true}    />
        <Scene key="HomeLoanDetails"  component={HomeLoanDetails} animation='fade' hideNavBar={true}    />
      
      
      </Scene>
      </Router>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
