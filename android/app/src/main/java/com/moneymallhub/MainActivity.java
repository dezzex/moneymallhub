package com.moneymallhub;
import android.os.Bundle;
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;

public class MainActivity extends ReactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);
        super.onCreate(savedInstanceState);
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "moneymallhub";
    }
}


//public class MainActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler, NavigationView.OnNavigationItemSelectedListener, PermissionAwareActivity {
//
//    private HelloFragment mViewFragment;
//    private ReactInstanceManager mReactInstanceManager;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//
//        SplashScreen.show(this,true);
//        super.onCreate(savedInstanceState);
//
////        setContentView(R.layout.activity_main);
////        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
////        setSupportActionBar(toolbar);
////        Bundle initialProps = new Bundle();
////
////        mViewFragment = new HelloFragment();
////        mViewFragment.addInitialProps(initialProps);
////
////        mReactInstanceManager =
////                ((MainApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
////
////        if (mViewFragment != null) {
////            mViewFragment.setMainApplication((ReactApplication) getApplication());
////            mViewFragment.setmReactInstanceManager(mReactInstanceManager);
////
////        }
////        getSupportFragmentManager().beginTransaction().add(R.id.container, mViewFragment,"Main_Frag").commit();
////
////        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
////        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
////                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
////        drawer.setDrawerListener(toggle);
////        toggle.syncState();
////
////        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
////        navigationView.setNavigationItemSelectedListener(this);
//
//
//    }
//    @Override
//    public void onBackPressed() {
//        isHomeScreen();
//    }
//    public boolean isHomeScreen() {
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//            return true;
//        } else {
//            mReactInstanceManager.onBackPressed();
//            return true;
//        }
//    }
//
//    @Override
//    public void invokeDefaultOnBackPressed() {
//        super.onBackPressed();
//    }
//
//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//
//
//        if(menuItem.getItemId() == R.id.nav_home){
//            WritableMap params = Arguments.createMap();
//            mReactInstanceManager.getCurrentReactContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
//                    .emit("showHome",params);
//            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//            drawer.closeDrawer(GravityCompat.START);
//            return true;
//
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
//    }
//
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//    }
//
//    /*
//     * Any activity that uses the ReactFragment or ReactActivty
//     * Needs to call onHostPause() on the ReactInstanceManager
//     */
//    @Override
//    protected void onPause() {
//        super.onPause();
//
//        if (mReactInstanceManager != null) {
//            mReactInstanceManager.onHostPause();
//        }
//    }
//
//    /*
//     * Same as onPause - need to call onHostResume
//     * on our ReactInstanceManager
//     */
//    @Override
//    protected void onResume() {
//        super.onResume();
//        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//
//        if (mReactInstanceManager != null) {
//            mReactInstanceManager.onHostResume(this, this);
//        }
//        if (mReactInstanceManager.getCurrentReactContext() == null) return;
//
//    }
//
//    private PermissionListener mPermissionListener;
//
//    @Override
//    public int checkPermission(String permission, int pid, int uid) {
//        return PackageManager.PERMISSION_GRANTED;
//    }
//
//    @Override
//    public  int checkSelfPermission(String permission) {
//        return PackageManager.PERMISSION_GRANTED;
//    }
//
//    @Override
//    public void requestPermissions(String[] permissions, int requestCode, PermissionListener listener) {
//        mPermissionListener = listener;
//        ActivityCompat.requestPermissions(this, permissions, requestCode);
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        // callback to native module
//        mPermissionListener.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//}
