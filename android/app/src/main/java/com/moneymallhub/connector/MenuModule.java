package com.moneymallhub.connector;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

import javax.annotation.Nonnull;


public class MenuModule extends ReactContextBaseJavaModule {

    private static final int READ_REQUEST_CODE = 41;


    public MenuModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void show(ReadableMap args, Callback callback) {
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        } else {
            intent = new Intent(Intent.ACTION_PICK);
        }
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (!args.isNull("filetype")) {
            ReadableArray filetypes = args.getArray("filetype");
            if (filetypes.size() > 0) {
                intent.setType(filetypes.getString(0));
            }
        }
        final Activity activity = (Activity) getReactApplicationContext().getCurrentActivity();

        // Return the fragment manager
        FragmentManager manager =  activity.getFragmentManager();
        Fragment frag = manager.findFragmentByTag("Main_Frag");
        frag.startActivityForResult(intent, READ_REQUEST_CODE, Bundle.EMPTY);


    }


    @Override
    public String getName() {
        return "MenuExample";
    }
}
