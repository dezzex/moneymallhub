package com.moneymallhub;

public class HelloFragment extends ReactFragment {
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public String getMainComponentName() {
        return "moneymallhub"; // name of our React Native component we've registered
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(mReactInstanceManager!=null){
            ((MainApplication) getActivity().getApplication()).getReactNativeHost().clear();
        }
    }
}
