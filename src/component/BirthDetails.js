import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';

import DatePicker from 'react-native-datepicker'
import { Actions } from 'react-native-router-flux';


export default class BirthDetails extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
                genderIndex:0,
                date:"2016-05-15"
        };
    }
    onGenderPressed(index){
            this.setState({genderIndex:index});
    }
    onContinuePressed(){
        Actions.JoiningDetails();
}

    
    componentWillReceiveProps(nextProps) {
        
    }
    
    render() {
        
        console.log('====================================');
        console.log(this.state.genderIndex);
        console.log('====================================');
        return (
            <View style={styles.container}>
                  <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Enter Your Details</Text>
            </View>
        <View style={{width:width(95),height:height(70),margin:width(2),marginTop:height(5),alignItems:"center",justifyContent:"center"}}>
            <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Choose your Gender</Text>
            {this.state.genderIndex === 0
            ?
            <View style={{width:width(95),height:height(8),flexDirection:"row"}}>
            <TouchableOpacity style={{width:width(40),height:height(8),margin: width(2),backgroundColor:"#016ed2",justifyContent:"center",alignItems:"center"}} onPress={this.onGenderPressed.bind(this,0)}>
                <Text  style={{color:"#fff",fontFamily: 'Roboto',fontSize: 18,fontWeight:"900"}}>
                    Male
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(40),height:height(8),margin: width(2),backgroundColor:"#d8d8d8",justifyContent:"center",alignItems:"center"}}  onPress={this.onGenderPressed.bind(this,1)}>
                <Text  style={{color:"#fff",fontFamily: 'Roboto',fontSize: 18,fontWeight:"900"}}>
                    Female
                </Text>
            </TouchableOpacity>
            </View>
            :
            <View style={{width:width(95),height:height(8),flexDirection:"row"}}>
            <TouchableOpacity style={{width:width(40),height:height(8),margin: width(2),backgroundColor:"#d8d8d8",justifyContent:"center",alignItems:"center"}} onPress={this.onGenderPressed.bind(this,0)}>
                <Text  style={{color:"#fff",fontFamily: 'Roboto',fontSize: 18,fontWeight:"900"}}>
                    Male
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(40),height:height(8),margin: width(2),backgroundColor:"#016ed2",justifyContent:"center",alignItems:"center"}}  onPress={this.onGenderPressed.bind(this,1)}>
                <Text  style={{color:"#fff",fontFamily: 'Roboto',fontSize: 18,fontWeight:"900"}}>
                    Female
                </Text>
            </TouchableOpacity>
            </View>
            }
           
            </View>
            <View style={{margin:height(2)}}>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Your Date of Birth</Text>
            <View style={{width:width(95),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
            <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="YYYY-MM-DD"
        minDate="2016-05-01"
        maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date: date})}}
      />
            </View>
            </View>
        <View style={{width:width(95),alignItems:"flex-end",justifyContent: 'flex-end',margin:height(2)}}>
            <TouchableOpacity style={{backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
        </View>
                   
        </View>      
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
    },
});
