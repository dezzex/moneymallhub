import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ImageBackground,
    Image,
    TouchableOpacity
} from 'react-native';

import { width, height, totalSize } from 'react-native-dimension';
import { Router, Scene, Actions } from 'react-native-router-flux';




export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onRegisterClicked(){
                // Actions.RegisterScreen();
    }
    render() {
        return (
            <ImageBackground style={{flex:1,alignItems:"flex-start",justifyContent:"flex-start"}} resizeMode='cover' source={require('../image/background.png')}>
                <View style={{height:"10%",width:"100%",alignItems:"center",justifyContent:"center",marginTop: height(60)}}>
              
                <TouchableOpacity  style={{width:"80%",height:"70%",borderRadius:25,backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}} onPress={this.onRegisterClicked.bind(this)}>
                    <Text style={{fontFamily:"Cochin",fontSize:22,color:"#54739b",fontWeight:"700"}}>
                        SignUp/Register
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity  style={{width:"80%",height:"40%"}} onPress={this.onRegisterClicked.bind(this)}>
                    
                </TouchableOpacity>
                <TouchableOpacity  style={{width:"80%",height:"70%",borderRadius:25,backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}} onPress={this.onRegisterClicked.bind(this)}>
                    <Text style={{fontFamily:"Cochin",fontSize:22,color:"#54739b",fontWeight:"700"}}>
                        Login
                    </Text>
                </TouchableOpacity>
                </View>
               
            </ImageBackground>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
