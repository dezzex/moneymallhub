import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';
import Timeline from 'react-native-timeline-listview'

export default class TrackOrder extends Component {

    constructor(props) {
        super(props);
        this.data = [
            { title: 'Confirmed Order', description: 'Your Order is Confirmed'},
            { title: 'Processing Order', description: 'Order Processing this may take 2-3 working days.',lineColor:'grey',circleColor:"grey"},
            {title: 'Dispatched Order', description: 'Order Disaptched for Delivery',lineColor:'grey',circleColor:"grey"},
            {title: 'Prodcut Delivered', description: 'Product Handed over',lineColor:'grey',circleColor:"grey"},
          ]
    }
    
    onBackPressed(){
        Actions.OrderConfirmation();
    }
    
    render() {
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
                    <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25,flexDirection:"row"}}>
                    <TouchableOpacity style={{width:width(10),height:height(8)}} onPress={this.onBackPressed.bind(this)}>
                    <Image source={require("../image/backarrow.png")} style={{width:width(10),height:height(8),resizeMode:"contain"}}/>
                    </TouchableOpacity>
                    <View style={{width:width(80),height:height(8),marginTop:height(1.5),marginLeft:width(2)}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Track Order</Text>
                    </View>

            </View>
            <View style={{width:width(100),height:height(85)}}>
            <TouchableOpacity style={{width:width(95),height:height(10),borderWidth:3,borderColor:"#016ed2",margin:height(2),alignItems:"center",justifyContent:"center"}}>
           <Text>
               Shipped Via : ABC Courier Service
               </Text>
               <Text>
               Status : Confirmed
               </Text>
               <Text>
               Expected Date: 12/5/2019
           </Text>
           </TouchableOpacity>
            <Timeline
          style={styles.list}
          data={this.data}
          circleSize={40}
          lineWidth={6}
          circleColor='#016ed2'
          lineColor='#016ed2'
          timeContainerStyle={{minWidth:52}}
          showTime={false}
          timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
          descriptionStyle={{color:'gray'}} 
          options={{
            style:{paddingTop:5},
          }}
        />
            </View>
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
    list: {
        flex: 1,
        marginTop:20,
        padding: 20,
            paddingTop:65,
      },
});
