import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';


export default class  extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onTrackOrderPressed(){
        Actions.TrackOrder();
    }
    onContinuePressed(){
        Actions.Main();
    }
    
    render() {
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Order Details</Text>
            </View>
            <View style={{width:width(100),height:height(100),alignItems:"center",justifyContent:"center"}}>

            <View style={{width:width(96),height:height(95),alignItems:"center",marginTop:height(15)}}>
            <View style={{alignItems:"center",justifyContent:"center",width:width(99),padding:width(2)}}>
            <Text style={{fontFamily:"Roboto",fontSize:16,alignItems:"center",justifyContent:"center",fontWeight:"500"}}>
                Congratulations! Your order has been confirmed will soon be delivered to you.
            </Text>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",width:width(99),padding:width(2)}}>
            <TouchableOpacity style={{width:width(95),height:height(20),borderWidth:2,borderColor:"#016ed2",alignItems:"center",justifyContent:"center",margin:height(2)}}>
            <View style={{width:width(95),height:height(3),padding:height(2)}}>
                <Text style={{fontFamily:"Roboto",alignItems:"center",justifyContent:"center",fontSize:16,fontWeight:"bold",color:"#016ed2"}}>
                    Application Number:4575451454412445
                </Text>
                </View>
                <View style={{width:width(95),height:height(17),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/re.jpg")} style={{resizeMode:"contain",width:width(35),height:height(15),margin:width(1)}}/>
                    <View style={{width:width(52),height:height(16),margin:width(1)}}>
                    <Text style={{fontFamily:"Roboto",fontSize:18,fontWeight:"900",color:"#000"}}>
                        FAB-Etihad Guest Infinite Card
                        -Credit Card 
                        1 Credit Card
                    </Text>
                    <Text style={{fontFamily:"Roboto",fontSize:18,fontWeight:"400",color:"#000"}}>
                        Order Has been confirmed
                    </Text>
                    </View>
                </View>
            </TouchableOpacity>
            </View>

            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(6),width:width(85),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={() => { this.onTrackOrderPressed() } }>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Track Your Order
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(6),width:width(85),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={() => { this.onContinuePressed() } }>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue Shopping
                </Text>
            </TouchableOpacity>
            </View>
            </View>
            
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
});
