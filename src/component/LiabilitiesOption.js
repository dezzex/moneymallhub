import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';


export default class LiabilitiesOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onContinuePressed(){
    Actions.LoanDocuments();
}
  render() {
    return (
        <View>
          <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
        <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Existing Liabilities</Text>
        </View>
        <View style={{width:width(99),height:height(45),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                <TouchableOpacity style={{width:width(40),height:height(18),alignItems:"center",justifyContent:"center",borderColor:"#016ed2",borderWidth:3,margin:width(2)}} onPress={this.onContinuePressed.bind(this)}>
                    <Image source={require("../image/card.png")} style={{width:width(40),height:height(13),resizeMode:"contain"}}/>
                    <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"#016ed2",width:width(40),height:height(5)}}>
                        <Text style={{color:"#fff",fontFamily:"Roboto",fontSize:18}}>Cards</Text>    
                    </View> 

                </TouchableOpacity>
                <TouchableOpacity style={{width:width(40),height:height(18),alignItems:"center",justifyContent:"center",borderColor:"#016ed2",borderWidth:3,margin:width(2)}} onPress={this.onContinuePressed.bind(this)}>
                    <Image source={require("../image/loan.png")} style={{width:width(40),height:height(13),resizeMode:"contain"}}/>
                    <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"#016ed2",width:width(40),height:height(5)}}>
                        <Text style={{color:"#fff",fontFamily:"Roboto",fontSize:18}}>Loans</Text>    
                    </View> 

                </TouchableOpacity>
        </View>
        </View>
    );
  }
}
