import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Picker,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';

import DatePicker from 'react-native-datepicker'
import { Actions } from 'react-native-router-flux';



export default class MartialDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status:0,
            email:''
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onContinuePressed(){
        Actions.LiabilitiesOption();
}
    render() {
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Enter Your Details</Text>
            </View>
            <View style={{height:height(85),width:width(90),marginTop:height(10),marginLeft: 25}}>
            <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Enter Your Email Address?</Text>
            <TextInput
                style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter your Email Address"}
                value={this.state.email}
                onChangeText={(text) => {this.setState({email:text})}}
            />
            </View>
            <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Your Martial Status</Text>
            <TouchableOpacity style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3}}>
           
               <Picker
                   selectedValue={this.state.status}
                   style={{height: height(10), width: width(85),borderColor:"#016ed2",borderWidth:3}}
                   onValueChange={(itemValue, itemIndex) =>
                       this.setState({status: itemValue})
                   }>
                    <Picker.Item label="Choose Your Martial Status" value="0" style={{fontFamily:"Roboto",fontSize:20}} />
                    <Picker.Item label="Single" value="1" style={{fontFamily:"Roboto",fontSize:20}} />
                    <Picker.Item label="Married" value="2" style={{fontFamily:"Roboto",fontSize:20}} />
                           
                   </Picker>
               </TouchableOpacity>
               </View>
               <View style={{width:width(99),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
        <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Back
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
           
        </View>
        </View>
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        // justifyContent: 'center',
        // alignItems: 'center',
    },
});
