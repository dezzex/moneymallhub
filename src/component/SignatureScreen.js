import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';


import SignatureCapture from 'react-native-signature-capture';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';


export default class  extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
    onContinuePressed(){
        Actions.DigitalSignature();
}
    
    componentWillReceiveProps(nextProps) {
        
    }
    saveSign() {
        // this.refs["sign"].saveImage();
        
        Actions.UploadPicture();
        
    }

    resetSign() {
        // this.refs["sign"].resetImage();
    }

    _onSaveEvent(result) {
        console.log(result);
        this.onContinuePressed();
    }
    _onDragEvent() {
        console.log("dragged");
    }
    
    render() {
        return (
            <View style={{ flex: 1, flexDirection: "column"}}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Consent</Text>
            </View>
            <View style={{alignItems:"center",justifyContent:"center"}}> 
            <View style={{width:width(100),height:height(8),paddingTop:10,paddingLeft: 25}}>
                <Text style={{fontSize:26,fontFamily:"Roboto",color:"#016ed2"}}>Digital Signature</Text>
        </View>
            <View style={{width:width(91),height:height(51),alignItems:"center",justifyContent:"center"}}>
           
            <SignatureCapture
                style={{width:width(90),height:height(50),borderWidth:1,borderColor:"grey",borderRadius:8,}}
                ref="sign"
                onSaveEvent={this._onSaveEvent}
                onDragEvent={this._onDragEvent}
                saveImageFileInExtStorage={false}
                showNativeButtons={false}
                showTitleLabel={false}
                viewMode={"portrait"}/>
                </View>
                     <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"flex-end",justifyContent:"flex-end"}}>
        <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={() => { this.resetSign() } }>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Reset
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={() => { this.saveSign() } }>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Save
                </Text>
            </TouchableOpacity>
            </View>
            </View>
        </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
