import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import FilePickerManager from 'react-native-file-picker';
import { Actions } from 'react-native-router-flux';

import * as MenuConnector from '../nativeconnector/menuconnector';


export default class LoanDocuments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            emiratesid:'',
            passport:'',
            visa:'',
            salary:'',
            statement:''
            
        };
    }
    onContinuePressed(){
        Actions.DigitalSignature();
}
    
    componentWillReceiveProps(nextProps) {
        
    }
    onDocumentUpload(idx){
        var self = this;
       
        DocumentPicker.show({
            filetype: [DocumentPickerUtil.allFiles()],
          },(error,res) => {

            
            console.log(
               res.uri,
               res.type, // mime type
               res.fileName,
               res.fileSize
            );
            if(res.fileName !== '' && idx === 0){
                    self.setState({emiratesid:res.fileName});
            }
            else if(res.fileName !== '' && idx === 1){
                self.setState({passport:res.fileName});
            }
            else if(res.fileName !== '' && idx === 2){
                self.setState({visa:res.fileName});
            }
            else if(res.fileName !== '' && idx === 3){
                self.setState({salary:res.fileName});
            }
            else if(res.fileName !== '' && idx === 4){
                self.setState({statement:res.fileName});
            }
          });
    }
    
    render() {
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Load Documents</Text>
            </View>
            <View style={{width:width(99),height:height(70)}}>
               <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(89),height:height(10)}}>
                <Text style={{color:"grey",fontSize:18,fontFamily:"Roboto"}}>
                    Emirates Id
                </Text>
                {this.state.emiratesid !== ''
                ?
                <View style={{width:width(50),height:height(5),flexDirection:"row"}}>
                
                <Image source={require("../image/uploadsuccess.png")} style={{width:width(8),height:height(5)}}/>
                <View style={{height:height(5),alignItems:"center",justifyContent:"center"}}>
                    <Text>{this.state.emiratesid}</Text>
                    </View>
                </View>
                :
                <View/>}
                </View>
                <TouchableOpacity onPress={this.onDocumentUpload.bind(this,0)}>
                <Image source={require("../image/addicon.png")} style={{width:width(10),height:height(10),resizeMode:"contain"}}/>
                </TouchableOpacity>
                </View> 

                <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(89),height:height(10)}}>
                <Text style={{color:"grey",fontSize:18,fontFamily:"Roboto"}}>
                    Passport
                </Text>
                {this.state.passport !== ''
                ?
                <View style={{width:width(50),height:height(5),flexDirection:"row"}}>
                
                <Image source={require("../image/uploadsuccess.png")} style={{width:width(8),height:height(5)}}/>
                <View style={{height:height(5),alignItems:"center",justifyContent:"center"}}>
                    <Text>{this.state.passport}</Text>
                    </View>
                </View>
                :
                <View/>}
                </View>
                <TouchableOpacity onPress={this.onDocumentUpload.bind(this,1)}>
                <Image source={require("../image/addicon.png")} style={{width:width(10),height:height(10),resizeMode:"contain"}}/>
                </TouchableOpacity>
                </View> 

                <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(89),height:height(10)}}>
                <Text style={{color:"grey",fontSize:18,fontFamily:"Roboto"}}>
                   Visa
                </Text>
                
                {this.state.visa !== ''
                ?
                <View style={{width:width(50),height:height(5),flexDirection:"row"}}>
                
                <Image source={require("../image/uploadsuccess.png")} style={{width:width(8),height:height(5)}}/>
                <View style={{height:height(5),alignItems:"center",justifyContent:"center"}}>
                    <Text>{this.state.visa}</Text>
                    </View>
                </View>
                :
                <View/>}
                </View>
                <TouchableOpacity onPress={this.onDocumentUpload.bind(this,2)}>
                <Image source={require("../image/addicon.png")} style={{width:width(10),height:height(10),resizeMode:"contain"}}/>
                </TouchableOpacity>
                </View> 

                <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(89),height:height(10)}}>
                <Text style={{color:"grey",fontSize:18,fontFamily:"Roboto"}}>
                    Salary
                </Text>
                
                {this.state.salary !== ''
                ?
                <View style={{width:width(50),height:height(5),flexDirection:"row"}}>
                
                <Image source={require("../image/uploadsuccess.png")} style={{width:width(8),height:height(5)}}/>
                <View style={{height:height(5),alignItems:"center",justifyContent:"center"}}>
                    <Text>{this.state.salary}</Text>
                    </View>
                </View>
                :
                <View/>}
                </View>
                <TouchableOpacity onPress={this.onDocumentUpload.bind(this,3)}>
                <Image source={require("../image/addicon.png")} style={{width:width(10),height:height(10),resizeMode:"contain"}}/>
                </TouchableOpacity>
                </View> 

                <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(89),height:height(10)}}>
                <Text style={{color:"grey",fontSize:18,fontFamily:"Roboto"}}>
                    Bank Statement
                </Text>
                
                {this.state.statement !== ''
                ?
                <View style={{width:width(50),height:height(5),flexDirection:"row"}}>
                
                <Image source={require("../image/uploadsuccess.png")} style={{width:width(8),height:height(5)}}/>
                <View style={{height:height(5),alignItems:"center",justifyContent:"center"}}>
                    <Text>{this.state.statement}</Text>
                    </View>
                </View>
                :
                <View/>}
                </View>
                <TouchableOpacity onPress={this.onDocumentUpload.bind(this,4)}>
                <Image source={require("../image/addicon.png")} style={{width:width(10),height:height(10),resizeMode:"contain"}}/>
                </TouchableOpacity>
                </View> 
            </View>
            <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
        <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Back
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
           
        </View>
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
    },
});
