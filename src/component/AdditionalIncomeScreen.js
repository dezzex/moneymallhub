import React, { Component } from 'react';

import { Text, View,StyleSheet,TextInput,TouchableOpacity,Image } from 'react-native'
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';


export default class AdditionalIncomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            income:''
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onContinuePressed(){
        Actions.MartialDetails();
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
      <TouchableOpacity style={{width:width(10),height:height(6)}}>
      <Image
            source={require('../image/drawer.png')}
            style={{ width: width(8), height: height(6)}}
          />
      </TouchableOpacity >
     
      <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
            </View>
      </View>
        <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Additional Income Details</Text>
        </View>
        <View style={{width:width(95),height:height(20),margin:width(2),marginTop:height(7)}}>
            <Text style={{color:"#000",fontSize:20,fontFamily:"Roboto"}}> Do you have any other source of income?</Text>
            <TextInput
                style={{width:width(95),height:height(10),marginBottom:height(2),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter Additional Income Source"}
                value={this.state.income}
                onChangeText={(text) => {this.setState({income:text})}}
            />
            <TextInput
                style={{width:width(95),height:height(10),marginBottom:height(2),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter Additional Income Source"}
                value={this.state.income}
                onChangeText={(text) => {this.setState({income:text})}}
            />
            <TextInput
                style={{width:width(95),height:height(10),marginBottom:height(2),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter Additional Income Source"}
                value={this.state.income}
                onChangeText={(text) => {this.setState({income:text})}}
            />
            <TextInput
                style={{width:width(95),height:height(10),marginBottom:height(2),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter Additional Income Source"}
                value={this.state.income}
                onChangeText={(text) => {this.setState({income:text})}}
            />
        </View>
        <View style={{width:width(95),height:height(45),alignItems:"flex-end",justifyContent: 'flex-end'}}>
            <TouchableOpacity style={{backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
        </View>
      </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
