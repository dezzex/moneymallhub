import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';
import { AnimatedCircularProgress } from 'react-native-circular-progress';


export default class FinalCreditScore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onMoreProductsPressed(){
        Actions.Main();
    }
    
    render() {
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
                   <View style={{width:width(100),height:height(8),paddingTop:10,paddingLeft: 25}}>
                <Text style={{fontSize:26,fontFamily:"Roboto",color:"#034293"}}>Your Credit Score</Text>
                     </View>
                     <View style={{width:width(95),height: height(35),marginTop:height(10),alignItems:"center",justifyContent:"center"}}>
                            
                     <AnimatedCircularProgress
                        size={300}
                        width={45}
                        backgroundWidth={45}
                        fill={60}
                        tintColor="#034293"
                        onAnimationComplete={() => console.log('onAnimationComplete')}
                        backgroundColor="#3d5875">
                                                    {
                                (fill) => (
                                <Text style={{fontSize:56,fontWeight:"900",color:"#034293"}}>
                                    590
                                </Text>
                                )
                            }
                        </AnimatedCircularProgress>
                        
                     </View>
                        <View style={{width:width(95),height:height(25),justifyContent:"flex-end"}}>
                        <TouchableOpacity style={{backgroundColor:"#016ed2",height:height(6),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onMoreProductsPressed.bind(this)}>
                                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                                BUY PRODUCTS
                            </Text>
                            </TouchableOpacity>
                            <Text style={{fontSize:16}}>
                                Learn hoe to increase your credit score and
                                maintaining a good credit score
                            </Text>
                            <TouchableOpacity>
                                <Text style={{fontSize:18}}>
                                CLICK HERE
                                </Text>
                            </TouchableOpacity>
                        </View>
                         
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
