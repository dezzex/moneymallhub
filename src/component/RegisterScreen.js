import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ImageBackground,
    TextInput,
    TouchableOpacity,
    Image,
    Modal,
    ToastAndroid
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import { width, height, totalSize } from 'react-native-dimension';


export default class RegisterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user:'',
            modalVisible:false,
            otp:''
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onSubmitClicked(){
           
    }
    onNextClicked(){
        Actions.VerificationScreen();
        //  this.setState({modalVisible:true})
    }
    
    render() {
        return (
            
            <ImageBackground style={{ flex: 1, }} resizeMode='cover' source={require('../image/glass_block1.jpg')}>
            <View style={styles.container}>
              <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          hardwareAccelerated={true}
          onRequestClose={() => {
            this.setState({modalVisible:false})
          }}>
          <View style={{flex:1,alignItems:'center', justifyContent:'center', backgroundColor:"rgba(59,59,59,0.4)"}}>
          <View style={{height:'20%', width:'80%', backgroundColor:'whitesmoke', alignContent:'center', justifyContent:'center'}}>
            <View>
                <TextInput
        style={{width:"90%",fontSize:22,fontWeight:"700"}}
        onChangeText={(otp) => this.setState({otp})}
        value={this.state.user}
        placeholder='Enter OTP'
      />
                <TouchableOpacity  style={{width:"100%",height:"25%",borderRadius:25,backgroundColor:"#1479ff",alignItems:"center",justifyContent:"center"}} onPress={this.onSubmitClicked.bind(this)}>
                    <Text style={{fontFamily:"Cochin",fontSize:22,color:"#fff",fontWeight:"700"}}>
                        SUBMIT
                    </Text>
                </TouchableOpacity>
            </View>
          </View>
          </View>
        </Modal>
            {/* <View style={{width:width(100),height:height(25),alignItems:"flex-start",justifyContent:"center"}}>
                <Text style={{fontSize:42,fontFamily:"Cochin",fontWeight:"700",color:"#ffff"}}>
                Complaint Tracker
            </Text>
            </View> */}

            <View style={{width:width(100),height:height(50),alignItems:"flex-start",justifyContent:"center"}}>
                 <Text style={{fontSize:26,fontFamily:"Cochin",fontWeight:"700",color:"#ffff"}}>
                Welcome !!
            </Text>
             <Text style={{fontSize:26,fontFamily:"Cochin",fontWeight:"700",color:"#ffff"}}>
               Let's Get Started!!
            </Text>
            </View>
            <View style={{width:width(100),height:height(20),alignItems:"center",justifyContent:"space-between"}}>
                 <TouchableOpacity style={{width:width(90),height:height(8),borderRadius:25,borderColor:"#ffff",borderWidth: 2,alignItems:"center",justifyContent:"center"}} onPress={this.onSubmitClicked}>
          <TextInput
        style={{width:"90%",color:"#ffff",fontSize:22,fontWeight:"700"}}
        onChangeText={(user) => this.setState({user})}
        value={this.state.user}
        placeholder='Enter Phone No or Email ID'
         placeholderTextColor="#ffff" 
      />
       </TouchableOpacity>      
        <TouchableOpacity style={{width:width(90),height:height(8),borderRadius:25,backgroundColor:"#ffff",alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
           <Text style={{color:"#1479ff",fontSize:22,fontFamily:"Cochin"}}>
              NEXT
           </Text>
       </TouchableOpacity>  

            </View>

            <View style={{width:width(100),height:height(15),flexDirection:"column",margin:5,alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(100),height:height(5),justifyContent:"center",alignItems:"center"}}>
                    <Text style={{fontSize:24,color:"#ffff",fontWeight:"700",fontFamily:"Cochin"}}>
                    OR
                </Text>
                </View>
                
                <View style={{width:width(100),height:height(8),flexDirection:"row",justifyContent:"space-between",alignItems:"center",margin:12}}>
                    <TouchableOpacity style={{width:width(25),height:height(8),borderRadius:25,backgroundColor:"#ffff",alignItems:"center",justifyContent:"center"}}>
                    <Image style={{width:width(25),height:height(8),resizeMode:"contain"}} source={require('../image/facebookicon.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(25),height:height(8),borderRadius:25,backgroundColor:"#ffff",alignItems:"center",justifyContent:"center"}}>
                    <Image style={{width:width(25),height:height(8),resizeMode:"contain"}} source={require('../image/mailicon.jpg')}/>
                </TouchableOpacity>
                </View>
                
            </View>
            </View>
            </ImageBackground>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
