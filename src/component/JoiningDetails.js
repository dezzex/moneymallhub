import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Picker,Image
} from 'react-native';
import {width,height} from 'react-native-dimension';

import DatePicker from 'react-native-datepicker'
import { Actions } from 'react-native-router-flux';


export default class JoiningDetails extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
                genderIndex:0,
                date:"2016-05-15",
                position:'',
                years:''
        };
    }
    onGenderPressed(index){
            this.setState({genderIndex:index});
    }
    onContinuePressed(){
        Actions.AdditionalIncome();
}

    
    componentWillReceiveProps(nextProps) {
        
    }
    
    render() {
        
        console.log('====================================');
        console.log(this.state.genderIndex);
        console.log('====================================');
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Enter Your Details</Text>
            </View>
            <View style={{width:width(95),height:height(85),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(95),alignItems:"center",justifyContent:"center"}}>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Date of Joining your Workplace</Text>
            <View style={{width:width(95),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
            
            <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="YYYY-MM-DD"
        minDate="2016-05-01"
        maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date: date})}}
      />
            </View>
            </View>
            <View style={{width:width(95),alignItems:"center",justifyContent:"center"}}>
                
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Your Destination</Text>
            <TouchableOpacity style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3}}>
           
               <Picker
                   selectedValue={this.state.position}
                   style={{height: height(10), width: width(85),borderColor:"#016ed2",borderWidth:3}}
                   onValueChange={(itemValue, itemIndex) =>
                       this.setState({position: itemValue})
                   }>
                    <Picker.Item label="Manager" value="Manager" style={{fontFamily:"Roboto",fontSize:20}} />
                    <Picker.Item label="Sr.Manager" value="Sr.Manager" style={{fontFamily:"Roboto",fontSize:20}} />
                    <Picker.Item label="Developer" value="Developer" style={{fontFamily:"Roboto",fontSize:20}} />
                           
                   </Picker>
               </TouchableOpacity>
               <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Choose No of Years</Text>
             <TextInput
                style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter your No. of Years"}
                value={this.state.years}
                onChangeText={(text) => {this.setState({years:text})}}
            />
            </View>
            
            </View>
        <View style={{width:width(99),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
        <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Back
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
           
        </View>
        </View>
        </View>      
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        height:height(95),
        width:width(95)
    },
});
