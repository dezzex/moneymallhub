import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Picker,
    Image
} from 'react-native';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';

const axios = require('axios');
var data = require('../component/assets/countrieslist.json'); 


export default class EmployeeDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            salary:'',
            empname:'',
            countries:'',
            countrieslist:[]
            
        };
    }

    componentDidMount(){
        // axios.get("../component/assets/countrieslist.json")
        // .then(function(response) {
        //     console.log('====================================');
        //     console.log(response);
        //     console.log('====================================');
        // })
            if(data.length > 0){
                this.setState({countrieslist:data})
            }
    }
    
    componentWillReceiveProps(nextProps) {
        
    } 
    onContinuePressed(){
            Actions.BirthDetails();
    }
    
    render() {
        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Employement Details</Text>
            </View>
        <View style={{width:width(95),height:height(70),margin:width(2),marginTop:height(5),alignItems:"center",justifyContent:"center"}}>
            <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>How much do you earn anually?</Text>
            <TextInput
                style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter your Salary"}
                value={this.state.salary}
                onChangeText={(text) => {this.setState({salary:text})}}
            />
            </View>
            <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Your Employer Name:(registered with bank)</Text>
             <TextInput
                style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3,fontFamily:"Roboto",fontSize:20}} 
                placeholder={"Enter your Employer Name"}
                value={this.state.empname}
                onChangeText={(text) => {this.setState({salary:text})}}
            />
            </View>
            <View>
            <Text style={{color:"#000",fontSize:18,fontFamily:"Roboto"}}>Select Your Nationality?</Text>
            <TouchableOpacity style={{width:width(85),height:height(10),borderColor:"#016ed2",borderWidth:3}}>
           
        {this.state.countrieslist !== ''
        ?
            <Picker
                selectedValue={this.state.countries}
                style={{height: height(10), width: width(85),borderColor:"#016ed2",borderWidth:3}}
                onValueChange={(itemValue, itemIndex) =>
                    this.setState({countries: itemValue})
                }>
                {this.state.countrieslist.map((val,idx) => {
                        return(
                            <Picker.Item key={idx} label={val.name} value={val.name} style={{fontFamily:"Roboto",fontSize:20}} />
                        )
                })}
                </Picker>
                :
            ""}
            </TouchableOpacity>
            </View>
        <View style={{width:width(95),alignItems:"flex-end",justifyContent: 'flex-end',margin:height(2)}}>
            <TouchableOpacity style={{backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
        </View>
                   
        </View>      
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
