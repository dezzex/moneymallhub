import React, { Component } from 'react';
import { View, Text,StyleSheet,Picker,ProgressBarAndroid,ScrollView,TouchableOpacity,Image } from 'react-native';
import {width,height} from 'react-native-dimension';
const axios = require('axios');
import AsyncStorage from '@react-native-community/async-storage';
import { Router, Scene, Actions } from 'react-native-router-flux';


var data = require('../component/assets/homeloanlist.json'); 

export default class HomeLoanList extends Component {
  constructor(props) {
    super(props);
    this.state = {

        data:[],
        isdataLoading:true

    };
  }

  onExploreClicked(value){
    
    Actions.HomeLoanDetails({card:value});
  }
  onApplyClicked(value){
      
    Actions.EmployeeDetails();
  }

  componentDidMount(){
    var self = this;

  
   
    try {
      AsyncStorage.getItem("core.app.homeloanlist").then((value) =>{
      var result = JSON.parse(value);
      if(result !== null){
        console.log('====================================');
        console.log("Result",result);
        console.log('====================================');
      
        this.setState({data:result,isdataLoading:false});
      }
      else{ 
        if(data.length > 0){
          self.setState({data:data,isdataLoading:false});
            try {
                AsyncStorage.setItem("core.app.homeloanlist",JSON.stringify(data));
            } catch (e) {
              // saving error
            }
          }
      }
    })
    } catch(e) {
      // error reading value
    }

       
  }

  render() {
    return (
      <View style={styles.container}>
      <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
        <View style={{width:width(98),height:height(8),flexDirection:"row",borderBottomWidth:1,borderColor: "grey",alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:24,color:"#000",fontFamily:"Roboto"}}>
                Category
            </Text>
        <View style={{width:width(50),borderWidth:1,borderColor:"grey",borderRadius:12,height:height(4),margin:width(1)}}>
        <Picker
            selectedValue={this.state.language}
            style={{width:width(50),height:height(4)}}
            onValueChange={(itemValue, itemIndex) =>
                this.setState({language: itemValue})
            }>
            
            
            <Picker.Item label="All Cards" value="1" />
            <Picker.Item label="All Home Loans" value="0" />
            <Picker.Item label="All Personal Loans" value="1" />
            
            <Picker.Item label="All Auto Loans" value="1" />
            </Picker>
       
        </View>
        

        </View>
        
        <View style={{width:width(98),borderColor:"grey",borderBottomWidth:1,height:height(8),alignItems:"center",alignItems:"center"}}>
                <Text style={{fontSize:18,color:"#034293",fontWeight:"600"}}>
                        Compare UAE's Top Financial Loans and find the best one for you.
                </Text>
        </View>
        
        <ScrollView style={{width:width(98)}}>
        {this.state.isdataLoading
        ?
        <View style={{width:width(95),height:height(65),alignItems:"center",justifyContent:"center"}}>
            
    <ProgressBarAndroid/>
        </View>
        :
            this.state.data.map((value,index) => {
                    return(
                        <TouchableOpacity key={index} style={{borderBottomWidth:1,borderColor:"grey",flexDirection:"row",height:height(22)}}>
                            <View style={{width:width(35),height:height(20),alignItems:"center",justifyContent:"center"}}>
                                    <Image source={require("../image/homeloanbanner.png")} resizeMode="contain" style={{width:width(35),height:height(18)}}/>
                            </View>
                            <View style={{width:width(65),height:height(20),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{fontSize:14,fontWeight:"900",color:"#000"}}>
                                        {value.name}
                                    </Text>
                                    <View style={{flexDirection:"row",width:width(60),height:height(3),alignItems:"flex-end",justifyContent:"flex-end"}}>
                                      <Text>
                                        4.7
                                      </Text>
                                      <Image source={require("../image/star.png")} style={{width:width(10),height:height(3)}} resizeMode="contain"/>
                                    </View>
                                    <View style={{height:height(7),width:width(30),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                      <View style={{height:height(5),width:width(30),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                                      <Text>
                                        Down Payment
                                      </Text>
                                      <Text style={{fontSize:14,fontWeight:"900",fontFamily:"Roboto"}}>
                                        {value.downpayment}
                                      </Text>
                                      </View>
                                      <View style={{height:height(5),width:width(30),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                                      <Text>
                                        Rate
                                      </Text>
                                      <Text  style={{fontSize:14,fontWeight:"900",fontFamily:"Roboto"}}>

                                        {value.redrate !== ''
                                        ?
                                        value.redrate
                                        :
                                          'NIL'
                                        }
                                      </Text>
                                      </View>

                                    </View>
                                    <View style={{height:height(4),width:width(65),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                       <TouchableOpacity style={{height:height(4),width:width(30),alignItems:"center",justifyContent:"center"}} onPress={this.onExploreClicked.bind(this,value)}>
                                       <Text style={{fontSize:18,color:"#034293",fontWeight:"500"}}>
                                          Explore
                                        </Text>
                                       </TouchableOpacity>
                                        
                                        <TouchableOpacity style={{width:width(30),height:height(4),alignItems:"center",justifyContent:"center",borderRadius:12,backgroundColor:"#034293"}} onPress={this.onApplyClicked.bind(this,value)}>
                                        <Text style={{fontSize:18,color:"#fff",fontWeight:"500"}}>
                                          Apply
                                        </Text>
                                        </TouchableOpacity>
                                       
                                     </View>
                            </View>

                        </TouchableOpacity>
                    )
            })
        }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
