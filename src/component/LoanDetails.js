import React, { Component } from 'react';

import { View, Text,StyleSheet,Picker,ProgressBarAndroid,ScrollView,TouchableOpacity,Image } from 'react-native';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';


export default class CardDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            index:0
        };
    }
    
    componentWillReceiveProps(nextProps) {
        
    }
    onOptionsPressed(index){
        this.setState({index:index})
    }
    onApplyClicked(){
        Actions.EmployeeDetails();
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(100),height:height(85),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(90),height:height(80),borderWidth:1,borderColor:"#cecccc",marginTop: height(2),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(90),height:height(40),alignItems:"center",justifyContent:"center",backgroundColor:"#cecccc"}}>
                                    <Image source={require("../image/bankbanner.png")} resizeMode="contain" style={{width:width(85),height:height(42)}}/>
                            </View>

                <View style={{width:width(90),height:height(10),alignItems:"flex-end",justifyContent:"flex-end"}}>
                <Text style={{fontSize:24,fontWeight:"900",fontFamily: 'Cochin'}}>
                    {this.props.card.name}
                </Text>
                </View>
                <View style={{flexDirection:"row",width:width(90),height:height(3),alignItems:"flex-end",justifyContent:"flex-end"}}>
                    <Text>
                      4.7
                     </Text>
                    <Image source={require("../image/star.png")} style={{width:width(10),height:height(3)}} resizeMode="contain"/>
                </View>
                <View style={{width:width(90),height:height(18)}}>

                    {this.state.index === 0
                    ?
                    <View>
                    <View style={{width:width(90),height:height(5),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    <TouchableOpacity style={{width:width(28),height:height(5),borderBottomWidth:2,borderBottomColor:"#565656",alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,0)}>
                    <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                        Details
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,1)}>
                    <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                         Features
                    </Text>
                </TouchableOpacity>
                 <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,2)}>
                    <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                        Our Opinion
                    </Text>
                </TouchableOpacity>
                </View>
                <View style={{width:width(90),height:height(18)}}>
                    <Text style={{fontSize:16,fontFamily:"Cochin"}}>
                        Min. Salary: {this.props.card.minsalary}
                    </Text>
                    <Text style={{fontSize:16,fontFamily:"Cochin"}}>
                        Pre-Approved: {this.props.card.preapproved}
                    </Text>
                    <Text style={{fontSize:16,fontFamily:"Cochin"}}>
                        Rate: {this.props.card.rate}
                    </Text>
                    <Text style={{fontSize:16,fontFamily:"Cochin"}}>
                        Minimum Salary: {this.props.card.minsalary}
                    </Text>
                    </View>
                    </View>
                    :
                    this.state.index === 1
                        ?
                     <View>   
                    <View style={{width:width(90),height:height(5),flexDirection:"row",alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,0)}>
                        <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                            Details
                        </Text>
                    </TouchableOpacity>
                    
                    
                    <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center",borderBottomWidth:2,borderBottomColor:"#565656"}} onPress={this.onOptionsPressed.bind(this,1)}>
                        <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                             Features
                        </Text>
                    </TouchableOpacity>
                     <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,2)}>
                        <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                            Our Opinion
                        </Text>
                    </TouchableOpacity>
                    </View>
                    <View style={{width:width(90),height:height(18)}}>
                    <Text style={{color:"#000",fontSize:16,fontFamily:"Cochin"}}>
                        Welcome Gift is provided with lot of benefits.
                    </Text>
                    <Text style={{color:"#000",fontSize:16,fontFamily:"Cochin"}}>
                       Elite Rewards is provided with lot of benefits.
                    </Text>
                    <Text style={{color:"#000",fontSize:16,fontFamily:"Cochin"}}>
                        Welcome Gift is provided with lot of benefits.
                    </Text>
                    </View>
                    </View>
                    :
                    <View>
                    <View style={{width:width(90),height:height(5),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                        <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,0)}>
                        <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                            Details
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center"}} onPress={this.onOptionsPressed.bind(this,1)}>
                        <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                             Features
                        </Text>
                    </TouchableOpacity>
                     <TouchableOpacity style={{width:width(28),height:height(5),alignItems:"center",justifyContent:"center",borderBottomWidth:2,borderBottomColor:"#565656"}} onPress={this.onOptionsPressed.bind(this,2)}>
                        <Text style={{fontSize:16,fontFamily:"Cochin",color:"#000",fontWeight:"500"}}>
                            Our Opinion
                        </Text>
                    </TouchableOpacity>
                    </View>
                        <View style={{width:width(90),height:height(18)}}>
                        <Text style={{fontSize:16,fontFamily:"Cochin"}}>
                            Welcome Board With Complimentaory Tockey.
                        </Text>
                        <Text style={{fontSize:16,fontFamily:"Cochin"}}>
                           Free Launch Access.
                        </Text>
                        </View>
                        </View>
                    }
                </View>
                <TouchableOpacity style={{backgroundColor:"#034293",width:width(40),height:height(5),borderRadius:20,alignItems:"center",justifyContent:"center"}} onPress={this.onApplyClicked.bind(this)}>
                    <Text style={{color:"#fff",fontWeight:"800",fontFamily:"Roboto",fontSize:26}}>
                            Apply
                    </Text>
                </TouchableOpacity>
            </View>
            </View>
            </View>
           
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
});
