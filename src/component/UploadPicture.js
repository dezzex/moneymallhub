import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {width,height} from 'react-native-dimension';
import { Actions } from 'react-native-router-flux';

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};



export default class  extends Component {

    constructor(props) {
        super(props);
        this.state = {
          avatarSource:''
        };
    }
    onContinuePressed(){
      Actions.OrderConfirmation();
}
    
    onUploadPressed(){
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          // const source = { uri: response.uri };
      
          // You can also display the image using data:
          const source = { uri: 'data:image/jpeg;base64,' + response.data };
      
          this.setState({
            avatarSource: source,
          });
        }
      });
    }
    
    render() {

      var icon = this.state.avatarSource !== ''
      ?
      this.state.avatarSource
      :
      require("../image/avatar.png");

        return (
            <View style={styles.container}>
            <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
               <View style={{width:width(100),height:height(8),backgroundColor:"#016ed2",paddingTop:10,paddingLeft: 25}}>
                    <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Upload Your Picture</Text>
            </View>
            <View style={{width:width(99),alignItems:"center",justifyContent:"center",height:height(85)}}>
            <Text style={{fontFamily:"Roboto",alignItems:"center",justifyContent:"center",fontSize:16}}>Make sure you're standing in front of a white background.</Text>
            <Image source={icon} style={{width:width(85),height:height(60),resizeMode:"contain"}}  />
            <View style={{width:width(99),height:height(12),marginLeft:width(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
        <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onUploadPressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Upload
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Continue
                </Text>
            </TouchableOpacity>
           
        </View>
        </View>
           
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
});
