import React, { Component } from 'react';

import { View, Text,StyleSheet,Picker,ProgressBarAndroid,ScrollView,TouchableOpacity,Image } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import {width,height} from 'react-native-dimension';
import {  Actions } from 'react-native-router-flux';


export default class VerificationScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            istimeout:false,
            firstCode:'',
            secondCode:'',
            thirdCode:'',
            fourthCode:'',
            fifthCode:'',
            sixthCode:''
            
        };
    }
    componentDidMount(){
        var self = this;
        setTimeout(function(){self.setState({istimeout: true})}, 5000)
    }
    componentWillReceiveProps(nextProps) {
        
    }
    onContinuePressed(){
        Actions.Main();
    }
    render() {
        return (
            <View style={styles.container}>
              <View  style={{width:width(100),height:height(8),backgroundColor:"#016ed2",flexDirection:"row",paddingTop:10,paddingLeft: 25}}>
                    <TouchableOpacity style={{width:width(10),height:height(6)}}>
                    <Image
                    source={require('../image/drawer.png')}
                    style={{ width: width(8), height: height(6)}}
                    />
                    </TouchableOpacity >
                    <View style={{width:width(80),paddingTop:5,paddingLeft:10,height:height(6)}}>
                        <Text style={{fontSize:26,fontFamily:"Roboto",color:"#fff"}}>Money Mall</Text>
                    </View>
            </View>
            <View style={{width:width(95),height:height(12),alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:26,color:"#035cad"}}>
                Verify Your Account
            </Text>
            <Text>
                OTP hab been sent to your number/mail ID
            </Text>
            <Text>
                Please enter it below
            </Text>


            </View>
            <View style={{width:width(99),height:height(10),flexDirection:"row",padding:3,alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{borderBottomWidth:3,borderBottomColor:"#016ed2",width:width(16.5),height:height(8),margin:5,alignItems:"center",justifyContent:"center"}}>
                    <TextInput style={{width:width(16.5),height:height(8),fontSize:20,alignItems:"center",justifyContent:"center"}} 
                        onSubmitEditing={() => { this.secondTextInput.focus(); }}
                        onChangeText={() => { this.secondTextInput.focus(); }}
                        
            maxLength={1}/>
                </TouchableOpacity>
                <TouchableOpacity style={{borderBottomWidth:3,borderBottomColor:"#016ed2",width:width(16.5),height:height(8),margin:5,alignItems:"center",justifyContent:"center"}}>
                    <TextInput style={{width:width(16.5),height:height(8),fontSize:20}}
                    onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                    onChangeText={() => { this.thirdTextInput.focus(); }}
                    ref={(input) => { this.secondTextInput = input; }}
                    
            maxLength={1}/>
                </TouchableOpacity>
                <TouchableOpacity style={{borderBottomWidth:3,borderBottomColor:"#016ed2",width:width(16.5),height:height(8),margin:5,alignItems:"center",justifyContent:"center"}}>
                    <TextInput style={{width:width(16.5),height:height(8),fontSize:20}}
                     ref={(input) => { this.thirdTextInput = input; }}
                     onSubmitEditing={() => { this.fourthTextInput.focus(); }}
                        onChangeText={() => { this.fourthTextInput.focus(); }}
                        
            maxLength={1}/>
                </TouchableOpacity> 
                <TouchableOpacity style={{borderBottomWidth:3,borderBottomColor:"#016ed2",width:width(16.5),height:height(8),margin:5,alignItems:"center",justifyContent:"center"}}>
                    <TextInput style={{width:width(16.5),height:height(8),fontSize:20}}
                     ref={(input) => { this.fourthTextInput = input; }}
                     onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                        onChangeText={() => { this.fifthTextInput.focus(); }}/>
                </TouchableOpacity>
                <TouchableOpacity style={{borderBottomWidth:3,borderBottomColor:"#016ed2",width:width(16.5),height:height(8),margin:5,alignItems:"center",justifyContent:"center"}}>
                    <TextInput style={{width:width(16.5),height:height(8),fontSize:20}}
                     ref={(input) => { this.fifthTextInput = input; }}
                     
            maxLength={1}/>
                </TouchableOpacity>
            </View>
            <View style={{width:width(99),height:height(20),margin:width(2)}}>
                <Text style={{width:width(99),height:height(8),alignItems:"center",justifyContent:"center"}}>
                    Didn't Receive OTP?
                </Text>
                <View style={{width:width(99),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                {this.state.istimeout
                ?
        <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Resend
                </Text>
            </TouchableOpacity>
            :
            <View style={{margin:width(2),backgroundColor:"#cecece",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                
            <Text style={{fontSize:18,fontFamily:"Roboto",color:"#000"}}>
                Resend
            </Text>
        </View>
                }
            <TouchableOpacity style={{margin:width(2),backgroundColor:"#016ed2",height:height(8),width:width(35),alignItems:"center",justifyContent:"center",borderRadius: 8}} onPress={this.onContinuePressed.bind(this)}>
                <Text style={{fontSize:18,fontFamily:"Roboto",color:"#fff"}}>
                    Apply
                </Text>
            </TouchableOpacity>
           
        </View>
                
            </View>
        
                <View />
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
});
